/******************************************************************************
 *AUTHOR giang.ngo
 *
 * NAME
 *   connect.js
 *
 * DESCRIPTION
 *   Chua cac function lien quan den viec ket noi toi oracle
 *
 *****************************************************************************/
var oracledb = require('oracledb');
const LOG_CLASS_TAG = 'connectOraDb:'

//hàm này trả về 1 connection pool
module.exports.createPool = function createPool(config) {
	return new Promise(function (resolve, reject) {
		try {
			oracledb.createPool(config)
				.then(function (pool) {
					resolve(pool)
				}, function (err) {
					err.message = LOG_CLASS_TAG + 'createPool:' + err.message
					sails.log.error("oracledb.createPool.:", err)
					reject(err)
				})
		} catch (e) {
			sails.log.error("createPool.:", e)
			reject(e)
		}
	})
}

//hàm này đóng connection pool
module.exports.terminatePool = function terminatePool(pool) {
	return new Promise(function (resolve, reject) {
		try {
			if (!pool) resolve('')
			pool.close(error => {
				if (error) {
					error.message = LOG_CLASS_TAG + 'terminatePool:' + error.message
					sails.log.error("oracledb.terminatePool.:", err)
					reject(error)
				} else {
					resolve('')
				}
			})
		} catch (e) {
			sails.log.error("terminatePool.:", e)
			reject(e)
		}
	})
}

module.exports.getConnection = function getConnection(pool) {
	return new Promise(function (resolve, reject) {
		try {
			pool.getConnection(function (err, connection) {
				if (err) {
					err.message = LOG_CLASS_TAG + 'getConnection:' + err.message
					sails.log.error("pool.getConnection.:", err)
					pool._logStats();
					reject(err);
				} else {
					sails.log.info("connection.getConnection.:DONE")
					resolve(connection);
				}
			});
		} catch (e) {
			sails.log.error("getConnection.:", e)
			reject(e)
		}
	});
}


module.exports.releaseConnection = function releaseConnection(connection) {
	return new Promise(function (resolve, reject) {
		try {
			connection.close(function (err) {
				if (err) {
					err.message = LOG_CLASS_TAG + 'releaseConnection:' + err.message
					sails.log.error("connection.releaseConnection.:", err)
					reject(err);
				} else {
					sails.log.info("connection.releaseConnection.:DONE")
					resolve('')
				}
			});
		} catch (e) {
			sails.log.error("releaseConnection.:", e)
			reject(e)
		}
	});


	// async.eachSeries(
	//     teardownScripts,
	//     function(statement, callback) {
	//         connection.execute(statement.sql, statement.binds, statement.options, function(err) {
	//             callback(err);
	//         });
	//     },
	//     function (err) {
	//         if (err) {
	//             console.error(err); //don't return as we still need to release the connection
	//         }

	//         connection.release(function(err) {
	//             if (err) {
	//                 console.error(err);
	//             }
	//         });
	//     }
	// );
}
/******************************************************************************
 *AUTHOR giang.ngo
 *
 * NAME
 *   connect.js
 *
 * DESCRIPTION
 *   Chua cac function lien quan den viec ket noi toi oracle
 *
 *****************************************************************************/
var db2 = require('ibm_db');
var Pool = db2.Pool
const LOG_CLASS_TAG = 'connectDB2:'
//hàm này trả về 1 connection pool
module.exports.createPool = function createPool(config) {
	return new Promise(function (resolve, reject) {
		try {
			pool = new Pool()
			pool.init(config.initPool, config.connectString);
			pool.setMaxPoolSize(config.maxPool);
			pool.open(config.connectString, function (err, connection) {
				if (err) {
					err.message = LOG_CLASS_TAG + 'openConnection:' + err.message
					sails.log.error("db2.openConnection.:", err)
					reject(err)
				}
				resolve(connection)
			});
			//resolve(pool)
		} catch (e) {
			sails.log.error("createPool.:", e)
			reject(e)
		}
	})

}
module.exports.releaseConnection = function releaseConnection(connection) {
	return new Promise(function (resolve, reject) {
		try {
			connection.close(function (err) {
				if (err) {
					err.message = LOG_CLASS_TAG + 'releaseConnection:' + err.message
					sails.log.error(LOG_CLASS_TAG + "connection.releaseConnection.:", err)
					reject(err);
				} else {
					sails.log.info(LOG_CLASS_TAG + "connection.releaseConnection.:DONE")
					resolve('')
				}
			});
		} catch (e) {
			sails.log.error(LOG_CLASS_TAG + "releaseConnection.:", e)
			reject(e)
		}
	});
}
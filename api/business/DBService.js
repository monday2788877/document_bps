var oracledb = require('oracledb');
var path = require('path')
var connectOra = require(path.resolve(__dirname, './connectOraDb.js'));
var LogHelper = require(path.resolve(__dirname, '../common/LogHelper'));
var fs = require('fs');


var pool;
// var buildupScripts = [];
// var teardownScripts = [];
module.exports.OBJECT = oracledb.OBJECT;
module.exports.STRING = oracledb.STRING;
module.exports.CURSOR = oracledb.CURSOR;
module.exports.BIND_OUT = oracledb.BIND_OUT;

module.exports.setPool = function setPool(poolPara) {
    pool = poolPara
}
var foCmdCache = [];
module.exports.setFoCmdCache = function setFoCmdCache(cmds) {
    sails.log.info("setFoCmdCache.:Size=", cmds.length);
    foCmdCache = cmds;
}
module.exports.getStatementCache = function getStatementCache(fundkey) {
    if (foCmdCache) {
        var record = foCmdCache.find(function (element) {
            return element.CMDCODE.toUpperCase() == fundkey.toUpperCase();
        });
        if (record) return record.CMDTEXT;
        return "ERR::::Not Statement";
    }
    return "ERR::::Not Statement Cache Init";
}
function getColname(metaData) {
    var col = [];
    for (var colname of metaData) {
        col.push(colname.name);
    }
    return col;
}

module.exports.execFunction = function execFunction(sql, bindParams, options) {
    // sails.log.debug(LogHelper.Start('DBService.execFunction', 'sql', sql, 'bindParams', bindParams, 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {

                    var results = await execute(sql, bindParams, options, conn);
                    // sails.log.debug(results);
                    var json = {};
                    if (bindParams.ret.type === oracledb.CURSOR) {
                        json.ret = {};
                        json.ret.col = getColname(results.outBinds.ret.metaData);
                        var rows = [];
                        var rowsArr = await fetchRowsFromRS(results.outBinds.ret, rows);
                        // sails.log.debug(LogHelper.Add('DBService.execFunction', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', rowsArr));
                        await connectOra.releaseConnection(conn);
                        if (rowsArr instanceof Error) {
                            reject(rowsArr);
                        } else {
                            json.ret.rows = rowsArr;
                            resolve(json);
                        }
                    } else {
                        json.ret = results.outBinds.ret;
                        await connectOra.releaseConnection(conn);
                        resolve(json);
                    }
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execFunction', sql, 'bindParams', bindParams), 'error', e)
            reject(e)
        }
    });
}
module.exports.execProcedureToDownloadBLOB = function execProcedureToDownloadBLOB(sql, bindParams, options) {
    // sails.log.debug(LogHelper.Start('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var results = await execute(sql, bindParams, options, conn);
                    // sails.log.debug(LogHelper.Add('DBService.execProcedureToDownloadBLOB', typeof (results)));
                    for (var attributename in bindParams) {
                        if (bindParams[attributename] && bindParams[attributename].type && bindParams[attributename].type === oracledb.CURSOR && bindParams[attributename].dir === 3003) {
                            var temp = {};
                            temp.col = getColname(results.outBinds[attributename].metaData);
                            var rows = [];
                            var rowsArr = await fetchRowsFromRS(results.outBinds[attributename], rows);
                            if (rowsArr instanceof Error) {
                                await connectOra.releaseConnection(conn);
                                reject(rowsArr);
                            } else {
                                temp.rows = rowsArr;
                                results.outBinds[attributename] = temp;
                            }
                        }
                    }

                    // tim xem trong bindParams co bien BLOB khong
                    var attrBLOB = null;
                    for (var attributename in bindParams) {
                        if (bindParams[attributename] && bindParams[attributename].type && bindParams[attributename].type === oracledb.BLOB && bindParams[attributename].dir === 3003) {
                            attrBLOB = attributename;
                        }
                    }
                    if (attrBLOB && results.outBinds[attrBLOB]) {
                        var lob = results.outBinds[attrBLOB];
                        lob.on('error', function (e) {
                            sails.log.error(LogHelper.Add('DBService.execProcedureToDownloadBLOB.lob.onError'), 'error', e)
                        });
                        var link = '.tmp/download/' + (bindParams[attrBLOB].name ? bindParams[attrBLOB].name : 'output') + '.jpg';
                        results.outBinds[attrBLOB] = link;
                        var outStream = fs.createWriteStream(link);
                        outStream.on('error', function (e) {
                            sails.log.error(LogHelper.Add('DBService.execProcedureToDownloadBLOB.outStream.onError'), 'error', e)
                        });
                        outStream.on('finish', async function () {
                            try {
                                sails.log.debug(LogHelper.Add('DBService.execProcedureToDownloadBLOB', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', results));
                                var commit = await conn.commit();
                                await connectOra.releaseConnection(conn);
                                resolve(results.outBinds);
                            } catch (err) {
                                await connectOra.releaseConnection(conn);
                                await lob.close();
                                reject(err);
                            }

                        });
                        lob.pipe(outStream);
                    } else {
                        sails.log.debug(LogHelper.Add('DBService.execProcedureToDownloadBLOB', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', results));
                        var commit = await conn.commit();
                        await connectOra.releaseConnection(conn);
                        resolve(results.outBinds);
                    }
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execProcedureToDownloadBLOB', sql, 'bindParams', bindParams), 'error', e)
            reject(e)
        }
    });
}
module.exports.execProcedure = function execProcedureSync(sql, bindParams, options) {
    // sails.log.debug(LogHelper.Start('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var results = await execute(sql, bindParams, options, conn);
                    // sails.log.debug(LogHelper.Add('DBService.execProcedure', typeof (results)));
                    for (var attributename in bindParams) {
                        if (bindParams[attributename] && bindParams[attributename].type && bindParams[attributename].type === oracledb.CURSOR) {
                            var temp = {};
                            temp.col = getColname(results.outBinds[attributename].metaData);
                            var rows = [];
                            var rowsArr = await fetchRowsFromRS(results.outBinds[attributename], rows);
                            if (rowsArr instanceof Error) {
                                await connectOra.releaseConnection(conn);
                                reject(rowsArr);
                            } else {
                                temp.rows = rowsArr;
                                results.outBinds[attributename] = temp;
                            }
                        } else if (bindParams[attributename] && bindParams[attributename].type && bindParams[attributename].type === oracledb.BUFFER) {
                          
                            results.outBinds[attributename] = results.outBinds[attributename].toString('base64');
                        }

                    }
                    // sails.log.debug(LogHelper.Add('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', results));
                    var commit = await conn.commit();
                    await connectOra.releaseConnection(conn);
                    resolve(results.outBinds);
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execProcedure', sql, 'bindParams', bindParams), 'error', e)
            reject(e)
        }
    });
}
module.exports.execProcedureToUpload = function execProcedureToUpload(sql, bindParams, fieldfile, uploadfile, options) {
    // sails.log.debug(LogHelper.Start('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'fieldfile', fieldfile, 'uploadfile is not null', (uploadfile != null), 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var templob = await conn.createLob(oracledb.BLOB, function (err, templob) {
                        if (!err) {
                            templob.on('error', async function (err) {
                                //somecallback(err);
                                sails.log.error(LogHelper.Add('DBService.execProcedureToUpload. Error when createLob'), 'error', err);
                                await connectOra.releaseConnection(conn);

                                reject(err);
                            });

                            // The data was loaded into the temporary LOB, so use it
                            templob.on('finish', async function () {
                                try {
                                    bindParams[fieldfile] = templob;
                                    var results = await execute(sql, bindParams, options, conn);
                                    sails.log.debug(LogHelper.Add('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', results));
                                    var commit = await conn.commit();
                                    await templob.close();
                                    await connectOra.releaseConnection(conn);
                                    resolve(results.outBinds);
                                } catch (err) {
                                    await connectOra.releaseConnection(conn);
                                    await templob.close();
                                    reject(err);
                                }

                            });

                            var inStream = fs.createReadStream(uploadfile.fd);
                            inStream.on('error', async function (err) {
                                await connectOra.releaseConnection(conn);
                                sails.log.error(LogHelper.Add('DBService.execProcedure. Error when read inStream'), 'error', err);

                                reject(err);
                            });
                            inStream.pipe(templob);
                        }
                    });
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execProcedureToUpload', sql, 'bindParams', bindParams), 'error', e)
            reject(e)
        }
    });
}
module.exports.execProcedureCB = function execProcedureCB(sql, bindParams, options, reqId, callback) {
    // sails.log.info(LogHelper.Start('DBService.execProcedure', 'sql', sql, 'bindParams', bindParams, 'options', options, 'reqId', reqId));
    try {
        if (!pool) {
            callback(new Error('No connection pool'));
        } else {
            oracledb.getConnection(function (err, conn) {
                if (err) {
                    sails.log.error(LogHelper.Add('DBService.execProcedureCB.oracledb.getConnection', sql, 'bindParams', bindParams, 'reqId', reqId), 'error', err)
                    oracledb.getPool()._logStats();
                    callback(err)
                } else {
                    // sails.log.info("oracledb.getConnection.:DONE", 'reqId', reqId)
                    try {
                        executeCB(sql, bindParams, options, conn, reqId, async function (results) {
                            // sails.log.debug(LogHelper.Add('DBService.execProcedure', typeof (results)));
                            if (results instanceof Error) {
                                // sails.log.info("oracledb.conn.close.:BEGIN")
                                conn.close(function (err) {
                                    sails.log.info("oracledb.conn.close.:END", err)
                                    callback(results);
                                })
                            } else {
                                for (var attributename in bindParams) {
                                    if (bindParams[attributename] && bindParams[attributename].type && bindParams[attributename].type === oracledb.CURSOR) {
                                        var temp = {};
                                        temp.col = getColname(results.outBinds[attributename].metaData);
                                        var rows = [];
                                        // sails.log.info("fetchRowsFromRS.:BEGIN")
                                        let rowsArr = await fetchRowsFromRS(results.outBinds[attributename], rows);
                                        // sails.log.info("fetchRowsFromRS.:DONE")
                                        if (rowsArr instanceof Error) {
                                            sails.log.error(LogHelper.Add('DBService.execProcedureCB', sql, 'bindParams', bindParams), 'error', rowsArr)
                                            conn.close(function (err) {
                                                callback(rowsArr);
                                            })
                                        } else {
                                            temp.rows = rowsArr;
                                            results.outBinds[attributename] = temp;
                                        }
                                    }
                                }
                                // sails.log.debug(LogHelper.Add('DBService.execProcedure.:Done', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', results));
                                // var commit = await conn.commit();
                                conn.commit(function (err) {
                                    conn.close(function (err) {
                                        callback(results.outBinds);
                                    })
                                })
                            }
                        });
                    } catch (e) {
                        sails.log.error(LogHelper.Add('DBService.execProcedureCB', sql, 'bindParams', bindParams, 'reqId', reqId), 'error', e)
                        conn.close(function (e) {
                            callback(new Error('ExecError.:' + e.message));
                        })
                    }
                }
            });
        }
    } catch (e) {
        sails.log.error(LogHelper.Add('DBService.execProcedureCB', sql, 'bindParams', bindParams, 'reqId', reqId), 'error', e)
        callback(e);
    }
}

module.exports.execQuerryTest = function execQuerryTest(sql, bindParams, options) {
    options.resultSet = true
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var results = await execute(sql, bindParams, options, conn);
                    var json = {};
                    json.col = getColname(results.metaData);
                    var rows = [];
                    var rowsArr = await fetchRowsFromRS(results.resultSet, rows);
                    await connectOra.releaseConnection(conn);
                    if (rowsArr instanceof Error) {
                        reject(rowsArr);
                    } else {
                        json.rows = rowsArr;
                        resolve(json);
                    }

                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error("execQuerryTest.:", sql, bindParams, e)
            reject(e)
        }
    });
}

module.exports.execQuerry = function execQuerry(sql, bindParams, options) {
    options.resultSet = true
    // sails.log.debug(LogHelper.Start('DBService.execQuerry', 'sql', sql, 'bindParams', bindParams, 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var results = await execute(sql, bindParams, options, conn);
                    var json = {};
                    json.col = getColname(results.metaData);
                    var rows = [];
                    var rowsArr = await fetchRowsFromRS(results.resultSet, rows);
                    // sails.log.debug(LogHelper.Add('DBService.execQuerry', 'sql', sql, 'bindParams', bindParams, 'options', options, 'results', rowsArr));
                    await connectOra.releaseConnection(conn);
                    if (rowsArr instanceof Error) {
                        reject(rowsArr);
                    } else {
                        json.rows = rowsArr;
                        resolve(json);
                    }
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execQuerry', sql, 'bindParams', bindParams), 'error', e)

            reject(e)
        }
    });
}
module.exports.execStatement = function execStatement(sql, bindParams, options) {
    // sails.log.debug(LogHelper.Start('DBService.execStatement', 'sql', sql, 'bindParams', bindParams, 'options', options));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    var results = await execute(sql, bindParams, options, conn);
                    // sails.log.debug(LogHelper.Add('DBService.execStatement', results));
                    var json = {};
                    json.rowsAffected = results.rowsAffected;
                    await conn.commit();
                    await connectOra.releaseConnection(conn);
                    resolve(json);
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execStatement', sql, 'bindParams', bindParams), 'error', e)

            reject(e)
        }
    });
}
module.exports.execArrayStatement = function execArrayStatement(sqlArr, bindVarArr) {
    // sails.log.debug(LogHelper.Start('DBService.execArrayStatement', 'sqlArr', sqlArr));
    return new Promise(async function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                var conn = await connectOra.getConnection(pool);
                try {
                    for (var index = 0; index < sqlArr.length; index++) {
                        var sql = sqlArr[index];
                        var bindVar = bindVarArr[index];
                        try {
                            await execute(sql, bindVar ? bindVar : {}, {}, conn);
                        } catch (error) {
                            sails.log.error("DBService.execArrayStatement fails", sql, error);
                            reject(error);
                            break;
                        }
                    }
                    await conn.commit();
                    await connectOra.releaseConnection(conn);
                    resolve('Success');
                } catch (err) {
                    await connectOra.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            reject(e)
        }
    });
}
async function fetchRowsFromRS(resultSet, rowsArr) {
    try {
        var rows = await resultSet.getRows(1000);
        if (rows.length > 0) {
            rows.forEach(function (row) {
                rowsArr.push(row);
            });
            if (rows.length === 1000)
                return await fetchRowsFromRS(resultSet, rowsArr);
            else {
                await resultSet.close();
                return rowsArr;
            }
        } else {
            await resultSet.close();
            return rowsArr;
        }
    } catch (e) {
        await resultSet.close();
        sails.log.error(LogHelper.Add('DBService.fetchRowsFromRS'), 'error', e)
        return e;
    }
}
module.exports.fetchRowsFromRS = fetchRowsFromRS


function addBuildupSql(statement) {
    var stmt = {
        sql: statement.sql,
        binds: statement.binds || {},
        options: statement.options || {}
    };

    buildupScripts.push(stmt);
}
module.exports.addBuildupSql = addBuildupSql;

function addTeardownSql(statement) {
    var stmt = {
        sql: statement.sql,
        binds: statement.binds || {},
        options: statement.options || {}
    };

    teardownScripts.push(stmt);
}
module.exports.addTeardownSql = addTeardownSql;



function execute(sql, bindParams, options, connection) {
    return new Promise(function (resolve, reject) {
        try {
            var start = process.hrtime();
            connection.execute(sql, bindParams, options, function (err, results) {
                let duration = LogHelper.getDuration(process.hrtime(start));
                sails.log.info(LogHelper.Add('DBService.execute', sql, 'duration', duration), (err) ? ('\nError:' + err) : '');
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        } catch (e) {
            sails.log.error(LogHelper.Add('DBService.execute', sql, 'bindParams', bindParams), 'error', e)
            reject(e)
        }
    });
}
function executeCB(sql, bindParams, options, connection, reqId, callback) {
    try {
        // sails.log.info("connection.executeCB.:BEGIN", 'reqId', reqId, sql, bindParams)
        var start = process.hrtime();
        connection.execute(sql, bindParams, options, function (err, results) {
            let duration = LogHelper.getDuration(process.hrtime(start));
            sails.log.info(LogHelper.Add('DBService.executeCB', sql, 'duration', duration), (err) ? ('\nError:' + err) : '');
            if (err) {
                callback(err);
            } else {
                // sails.log.info("connection.executeCB.:DONE", 'reqId', reqId, sql, bindParams)
                callback(results);
            }
        });
    } catch (e) {
        sails.log.error(LogHelper.Add('DBService.executeCB', sql, 'bindParams', bindParams), 'error', e)
        callback(e)
    }
}
// module.exports.execute = execute;








var path = require('path')
var connectDB2 = require(path.resolve(__dirname, './connectDB2.js'));
var pool
module.exports.setPool = function setPool(poolPara) {
    pool = poolPara
}
module.exports.query = async function query(sql, bindParams) {
    return new Promise(function (resolve, reject) {
        try {
            if (!pool) {
                reject(new Error('No connection pool'));
            } else {
                let conn = pool
                try {
                    // let conn = await connectDb2.openConnection(pool);

                    for (var attributename in bindParams) {
                        sql = sql.split('$' + attributename).join(bindParams[attributename].toString());
                    }
                    sails.log.info('DBService2.query sql:::', sql)
                    conn.query(sql, function (err1, rows) {
                        if (err1) {
                            connectDB2.releaseConnection(conn);
                            reject(err1);
                        }
                        else {
                            connectDB2.releaseConnection(conn);
                            resolve(rows);
                        }
                    })
                } catch (err) {
                    connectDB2.releaseConnection(conn);
                    reject(err);
                }
            }
        } catch (e) {
            sails.log.error('DBService2.query', sql, 'bindParams', bindParams, 'error', e)
            reject(e)
        }
    });
}










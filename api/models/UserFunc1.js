/**
 * UserFunc.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		USERID: {
			type: 'string'
		},
		TLID: {
			type: 'string'
		},
		ROLES: {
			type: 'string'
		},
		ROLECODE: {
			type: 'string'
		},
		USERID: {
			type: 'string'
		},
		OBJNAME: {
			type: 'string'
		},
		CMDNAME: {
			type: 'string'
		},
		EN_CMDNAME: {
			type: 'string'
		},
		ISINQUIRY: {
			type: 'string'
		},
		ISADD: {
			type: 'string'
		},
		ISEDIT: {
			type: 'string'
		},
		ISDELETE: {
			type: 'string'
		},
		ISAPPROVE: {
			type: 'string'
		},
		ODRID: {
			type: 'string'
		},
		CMDCODE: {
			type: 'string'
		},
		CMDID: {
			type: 'string'
		},
		PRID: {
			type: 'string'
		},
		LEV: {
			type: 'string'
		},
		LAST: {
			type: 'string'
		},
		IMGINDEX: {
			type: 'string'
		},
		MODCODE: {
			type: 'string'
		},
		MENUTYPE: {
			type: 'string'
		},
		CMDALLOW: {
			type: 'string'
		},
		AUTHCODE: {
			type: 'string'
		},
		RIGHTSCOPE: {
			type: 'string'
		},
		SHORTCUT: {
			type: 'string'
		}
	},
	updateOrCreate: function (criteria, values) {
		var self = this; // reference for use by callbacks

		// If no values were specified, use criteria
		if (!values) values = criteria.where ? criteria.where : criteria;

		return this.findOne(criteria).then(function (result) {
			if (result) {
				return self.update(criteria, values);
			} else {
				return self.create(values);
			}
		});
	},
	syncCreate: function (values) {
		var self = this; // reference for use by callbacks
		return self.create(values).then((record) => {
			return record;
		});
	},
};


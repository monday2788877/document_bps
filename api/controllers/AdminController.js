/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


var path = require('path')
var DBService = require(path.resolve(__dirname, '../business/DBService.js'));
var LogHelper = require(path.resolve(__dirname, '../common/LogHelper'));

module.exports = {
  loadStatement: async function (req) {
    sails.log.debug(LogHelper.Start('AdminController.loadStatement'), LogHelper.logReq(req));
    try {
      var sqlQrr = 'BEGIN FOPKS_SA.PRC_FOCMDCODE(:ret,:err_code,:err_msg); END;';
      var bindVar = {
        ret: { dir: 3003, type: 2004 },
        err_code: { dir: 3003, type: 2001 },
        err_msg: { dir: 3003, type: 2001 }
      }
      var result = await DBService.execProcedure(sqlQrr,
        bindVar,
        {});
      if (result.err_code === '0') { //giang.ngo: check ma loi cua db tra ra
        await FoCmdCode.destroy();
        var cmds = [];
        for (var data of result.ret.rows) {
          var col = result.ret.col;
          var obj = {};
          for (var index in col) {
            obj[col[index]] = data[index];
          }
          await FoCmdCode.create(obj);
          cmds.push(obj);
        }
        DBService.setFoCmdCache(cmds);
        return result;
      } else {
        return result;
      }

    } catch (err) {
      sails.log.error(LogHelper.End('AdminController.loadStatement'), LogHelper.logReq(req, err));
      return err;
    }
  },
  loadStatementApi: async function (req, res) {
    var rs = await this.loadStatement(req);
    return res.json(RestfullService.genResult(0, '', rs));

  },
  getListStatement: function (req, res) {
    FoCmdCode.find().exec((err, list) => {
      if (err) {
        return res.send(err);
      }
      return res.send(list);
    })
  },
  loadUsersFuncCB1: async function (req) {
    sails.log.debug(LogHelper.Start('loadUsersFuncCB1.loadStatement'), LogHelper.logReq(req));
    try {
      var sqlQrr = 'BEGIN FOPKS_SA.PRC_USER_FUNC1(:ret,:user,:err_code,:err_msg); END;';
      var bindVar = {
        ret: { dir: 3003, type: 2004 },
        user:'',
        err_code: { dir: 3003, type: 2001 },
        err_msg: { dir: 3003, type: 2001 }
      }
      var result = await DBService.execProcedure(sqlQrr,
        bindVar,
        {});
      if (result.err_code === '0') { //giang.ngo: check ma loi cua db tra ra
        await UserFunc1.destroy();
        await UserFunc1.create(result.DT);
        return result;
      } else {
        return result;
      }

    } catch (err) {
      sails.log.error(LogHelper.End('loadUsersFuncCB1.loadStatement'), LogHelper.logReq(req, err));
      return err;
    }
  },
};


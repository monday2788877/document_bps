/**
 * ExecStatementController
 *
 * @description :: Server-side logic for managing execstatements
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var path = require('path')
var DBService = require(path.resolve(__dirname, '../business/DBService.js'));
var mapStt = require(path.resolve(__dirname, '../../mapStatement/mapStt.js'));
var LogHelper = require(path.resolve(__dirname, '../common/LogHelper'));
var oracledb = require('oracledb');

async function getStatement(fundkey) {
      var record = await FoCmdCode.findOne({ CMDCODE: fundkey });
      if (record) return record.CMDTEXT;
      return "Not Statement";
}

function removePropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      for (var property in obj) {
            if (sqlQrr.indexOf(':' + property.toLowerCase() + ',') === -1 && sqlQrr.indexOf(':' + property.toLowerCase() + ')') === -1 && sqlQrr.indexOf(':' + property.toLowerCase() + ' ') === -1) {
                  delete obj[property];
            }
      }
      return obj;
}
function validatePropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      sqlQrr = sqlQrr.replace(':=', '');
      for (var property in obj) {
            var re = new RegExp((':' + property.toLowerCase()), 'g');
            sqlQrr = sqlQrr.replace(re, '');
      }
      return sqlQrr.indexOf(':') < 0;
}

function getPropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      sqlQrr = sqlQrr.replace(':=', '');
      for (var property in obj) {
            var re = new RegExp((':' + property.toLowerCase()), 'g');
            sqlQrr = sqlQrr.replace(re, '');
      }
      var regEx = new RegExp(/:(.*?),/g);
      return sqlQrr.match(regEx);
}
module.exports = {
      execSttQuerry: async function (req, res, next) {
            let {sqlQrr} = req.body;
            var bindVar = req.body.bindvar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execSttQuerry', 'sqlQrr', sqlQrr, 'bindVar', bindVar));
            try {
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execSttQuerry'), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execQuerry(sqlQrr,
                        bindVar,
                        {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execSttQuerry',  'sqlQrr', sqlQrr), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var restResult = RestfullService.genResult(0, "Success", result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execSttQuerry',  'sqlQrr', sqlQrr, '\nresult', result));
                        return res.send(restResult);
                  }
            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execSttQuerry', 'sqlQrr', sqlQrr), "\nError", LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execArrayStatement: async function (req, res) {
            var funckeyArr = req.body.funckey;
            var bindVarArr = req.body.bindvar;
            sails.log.info('execArrayStatement:.START', LogHelper.logReq(req));
            try {
                  for (var index = 0; index < funckeyArr.length; index++) {
                        var sql = funckeyArr[index];
                        var bindVar = bindVarArr[index];
                        bindVar = removePropertyNotExistInSQL(bindVar, sql);
                        bindVarArr[index] = bindVar
                  }
                  var result = await DBService.execArrayStatement(funckeyArr, bindVarArr, {});
                  sails.log.info('execArrayStatement:. api response', result.EC, result.EM, LogHelper.logReq(req));
                  return res.send(RestfullService.genResult(0, "Success", result));
            } catch (err) {
                  sails.log.error('execArrayStatement', LogHelper.logReq(req), err);
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execQuerry: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body.bindvar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));
            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execQuerry(sqlQrr,
                        bindVar,
                        {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var restResult = RestfullService.genResult(0, "Success", result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                        return res.send(restResult);
                  }
            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execQuerry', 'header', req.headers.fundservheader, 'funckey', funckey), "\nError", LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execStatement: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body.bindVar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));

            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execStatement(sqlQrr,
                        bindVar, {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var restResult = RestfullService.genResult(0, "Success", result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                        return res.send(restResult);
                  }

            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execStatement', 'header', req.headers.fundservheader, 'funckey', funckey), "\nError", LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execFunction: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body.bindvar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execFunction', 'header', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));

            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execFunction', 'header', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execFunction', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execFunction(sqlQrr,
                        bindVar,
                        {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execFunction', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var restResult = RestfullService.genResult(0, "Success", result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execFunction', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                        return res.send(restResult);
                  }

            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execFunction', req.headers.fundservheader, 'funckey', funckey), "\nError", LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execProcedureToDownloadBLOB: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body.bindvar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));
            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execProcedureToDownloadBLOB(sqlQrr,
                        bindVar,
                        {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var EC = 0;
                        if (result.err_code) EC = parseInt(result.err_code);
                        var EM = '';
                        if (result.err_msg) EM = result.err_msg;
                        var restResult = RestfullService.genResult(EC, EM, result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                        return res.send(restResult);
                  }
            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execProcedureToDownloadBLOB', req.headers.fundservheader, 'funckey', funckey), '\nError', LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execProcedure: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body.bindvar;
            sails.log.debug(LogHelper.Start('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));
            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                        return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  var result = await DBService.execProcedure(sqlQrr,
                        bindVar,
                        {});
                  if (typeof (result) == 'Error') {
                        var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                        return res.send(restResult);
                  } else {
                        var EC = 0;
                        if (result.err_code) EC = parseInt(result.err_code);
                        var EM = '';
                        if (result.err_msg) EM = result.err_msg;
                        var restResult = RestfullService.genResult(EC, EM, result);
                        sails.log.debug(LogHelper.End('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                        return res.send(restResult);
                  }
            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execProcedure', req.headers.fundservheader, 'funckey', funckey), '\nError', LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      execProcedureToUpload: async function (req, res, next) {
            var funckey = req.body.funckey;
            var bindVar = req.body;
            sails.log.debug(LogHelper.Start('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey, 'bindVar', bindVar));
            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: fundkey khong ton tai');
                        sails.log.error(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey), 'Error: fundkey khong ton tai', sqlQrr);
                        return res.send(restResult);
                  }
                  if (req._fileparser.upstreams.length == 0) {
                        let restResult = RestfullService.genResult(-101333, 'BPSService Error: No file was uploaded');
                        return res.send(restResult);
                  }
                  var fieldFile = req._fileparser.upstreams[0].fieldName;

                  req.file(fieldFile).upload({}, async function (err, uploads) {
                        if (err) {
                              sails.log.error(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey), 'Error', LogHelper.logReq(req, err));
                              let restResult = RestfullService.genResult(-101333, 'BPSService Error', LogHelper.logReq(req, err));
                              return res.send(restResult);
                        }
                        if (uploads.length === 0) {
                              let restResult = RestfullService.genResult(-101333, 'BPSService Error: No file was uploaded');
                              return res.send(restResult);
                        }
                        sails.log.info(LogHelper.Add('execProcedureToUpload: upload.fd', uploads[0].fd, 'upload.filename', uploads[0].filename, 'fieldFile', fieldFile));
                        bindVar[fieldFile] = '';

                        bindVar.err_code = { dir: 3002, type: 2001, value: 0 };
                        bindVar.err_msg = { dir: 3003, type: 2001 };
                        if (!bindVar.REFLOGID) {
                              bindVar.REFLOGID = funckey + '.no reflogid';
                        }
                        if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                              let restResult = RestfullService.genResult(-101333, 'BPSService Error: not all variable bounds=== ' + getPropertyNotExistInSQL(bindVar, sqlQrr));
                              sails.log.error(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey), 'Error: not all variable bounds', sqlQrr);
                              return res.send(restResult);
                        }
                        bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                        var result = await DBService.execProcedureToUpload(sqlQrr, bindVar, fieldFile, uploads[0], {});

                        if (typeof (result) == 'Error') {
                              var restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
                              sails.log.error(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey), '\nError', result);
                              return res.send(restResult);
                        } else {
                              var EC = 0;
                              if (result.err_code) EC = parseInt(result.err_code);
                              var EM = '';
                              if (result.err_msg) EM = result.err_msg;
                              var restResult = RestfullService.genResult(EC, EM, result);
                              sails.log.debug(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey, '\nresult', result));
                              return res.send(restResult);
                        }

                  });
            } catch (err) {
                  sails.log.error(LogHelper.End('ExecStatementController.execProcedureToUpload', req.headers.fundservheader, 'funckey', funckey), '\nError', LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
      // execProcedure: async function (req, res, next) {
      //       var funckey = req.body.funckey;
      //       var bindVar = req.body.bindvar;
      //       var start = process.hrtime();
      //       sails.log.debug(LogHelper.Start(req.headers.fundservheader, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar));
      //       try {
      //             let sqlQrr = DBService.getStatementCache(funckey);
      //             if (sqlQrr.indexOf("ERR::::") == 0) {
      //                   let restResult = RestfullService.genResult(-101333, 'BPSService Error', sqlQrr);
      //                   let end = LogHelper.getDuration(process.hrtime(start)[1]);
      //                   sails.log.error(LogHelper.End(req.headers.fundservheader, end, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar), 'error', sqlQrr);
      //                   return res.send(restResult);
      //             }
      //             bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
      //             DBService.execProcedureCB(sqlQrr,
      //                   bindVar,
      //                   {}, sqlQrr + ":::::" + start, (result) => {
      //                         if (typeof (result) == 'Error') {
      //                               let restResult = RestfullService.genResult(-101333, 'BPSService Error', result);
      //                               let end = LogHelper.getDuration(process.hrtime(start)[1]);
      //                               sails.log.error(LogHelper.End(req.headers.fundservheader, end, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar), 'error', result);
      //                               return res.send(restResult);
      //                         } else {
      //                               let EC = 0;
      //                               if (result.err_code) EC = parseInt(result.err_code);
      //                               let EM = '';
      //                               if (result.err_msg) EM = result.err_msg;
      //                               let restResult = RestfullService.genResult(EC, EM, result);
      //                               sails.log.debug(LogHelper.Add(req.headers.fundservheader, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar, '\nresult', result));
      //                               let end = LogHelper.getDuration(process.hrtime(start)[1]);
      //                               sails.log.info(LogHelper.End(req.headers.fundservheader, end, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar));
      //                               return res.send(restResult);
      //                         }
      //                   });

      //       } catch (err) {
      //             let end = LogHelper.getDuration(process.hrtime(start)[1]);
      //             sails.log.error(LogHelper.End(req.headers.fundservheader, end, 'ExecStatementController.execProcedure', 'funckey', funckey, 'bindVar', bindVar), "\nError", LogHelper.logReq(req, err));
      //             return res.send(RestfullService.genResult(-101333, err.message, null));
      //       }
      // },
      testDB: async function (req, res, next) {
            sails.log.info("testDB BEGIN");
            try {
                  var sqlQrr = "SELECT sysdate from dual";
                  var result = await DBService.execQuerry(sqlQrr, {}, {});
                  var restResult = RestfullService.genResult(0, "Success", result);
                  sails.log.info("testDB Result.:" + restResult);

                  // oracledb.getPool()._logStats();
                  // sqlQrr = "select  level from dual connect by level <= 1001";
                  // result = await DBService.execQuerry(sqlQrr, {}, {});
                  // sails.log.info("testDBLarge Result.:" + result.rows.length);

                  // sqlQrr = "select  level from dual connect by level <= 10000";
                  // result = await DBService.execQuerry(sqlQrr, {}, {});
                  // sails.log.info("testDBLarge Result.:" + result.rows.length);

                  switch (reqTestCount++ % 4) {
                        case 0:
                              sqlQrr = "select  level from dualllll connect by level <= 1000000";
                              result = await DBService.execQuerry(sqlQrr, {}, {});
                              sails.log.info("testDBLarge Result.:" + result.rows.length);
                              break;
                        case 1:
                              sqlQrr = "select to_number(sysdate) from dual";
                              result = await DBService.execQuerry(sqlQrr, {}, {});
                              sails.log.info("testDBLarge Result.:" + result.rows.length);
                              break;
                        case 2:
                              sqlQrr = "select to_number(:abc) from dual";
                              result = await DBService.execQuerry(sqlQrr, { abc: "1000" }, {});
                              sails.log.info("testDBBind Result.:" + result.rows.length);
                              break;
                        case 3:
                              sqlQrr = "select to_number(:abc),:aaaa test from dual";
                              result = await DBService.execQuerry(sqlQrr, { abc: "1000" }, {});
                              sails.log.info("testDBLarge Result.:" + result.rows.length);
                              break;
                  }
                  // sleep(10000)
                  // sails.log.info("testDB Result.:END");
                  return res.send(restResult);
            } catch (err) {
                  sails.log.error("testDB ERROR.:", LogHelper.logReq(req, err));
                  return res.send(RestfullService.genResult(-101333, err.message, null));
            }
      },
};
var reqTestCount = 0;
function sleep(time, callback) {
      var stop = new Date().getTime();
      while (new Date().getTime() < stop + time) {
            ;
      }
      if (callback)
            callback();
}

/**
 * ExecStatementController
 *
 * @description :: Server-side logic for managing execstatements
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var path = require('path')
var DBService = require(path.resolve(__dirname, '../business/DBService.js'));
var DB2Service = require(path.resolve(__dirname, '../business/DB2Service.js'));
var LogHelper = require(path.resolve(__dirname, '../common/LogHelper'));
var prefix='ExecStatementDB2Controller'
async function getStatement(fundkey) {
      var record = await FoCmdCode.findOne({ CMDCODE: fundkey });
      if (record) return record.CMDTEXT;
      return "Not Statement";
}

function removePropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      for (var property in obj) {
          if (sqlQrr.indexOf('$' + property.toLowerCase()) < 0) {
              delete obj[property];
          }
      }
      return obj;
  }
  function validatePropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      for (var property in obj) {
          sqlQrr = sqlQrr.split('$' + property.toLowerCase()).join('');
      }
      return sqlQrr.indexOf('$') < 0;
  }
  
  async function getPropertyNotExistInSQL(obj, sqlQrr) {
      sqlQrr = sqlQrr.toLowerCase();
      let errMsg = ''
      for (var property in obj) {
          if (sqlQrr.indexOf(('$' + property.toLowerCase())) < 0) errMsg += property + ','
  
      }
  
      return errMsg
  }
  
module.exports = {
      execSttQuerry: async function (req, res, next) {
            let {funckey,bindVar} = req.body;
            sails.log.info(LogHelper.Start('ExecStatementController.execSttQuerry', 'funckey', funckey, 'bindVar', bindVar));
            try {
                  let sqlQrr = DBService.getStatementCache(funckey);
                  if (sqlQrr.indexOf("ERR::::") == 0) {
                      let restResult = RestfullService.genResult(-101333, 'BPSService_DB2 Error', 'fundkey khong ton tai');
                      sails.log.error(prefix + 'Call exec query', 'funckey', funckey, 'Error: fundkey khong ton tai', sqlQrr);
                      return res.send(restResult);
                  }
                  bindVar = removePropertyNotExistInSQL(bindVar, sqlQrr);
                  if (!validatePropertyNotExistInSQL(bindVar, sqlQrr)) {
                      let PropertyNotExistInSQL = await getPropertyNotExistInSQL(bindVar, sqlQrr)
                      let restResult = await RestfullService.genResult(-101333, 'BPSService_DB2 Error', 'not all variable bounds===' + PropertyNotExistInSQL);
                      sails.log.error(prefix + 'Call exec query', 'funckey', funckey, 'Error: not all variable bounds', sqlQrr);
                      return res.send(restResult);
                  }
                  var result = await DB2Service.query(sqlQrr,
                      bindVar,
                  );
                  if (typeof (result) == 'Error') {
                      var restResult = RestfullService.genResult(-101333, 'BPSService_DB2 Error', result);
                      sails.log.error(prefix + 'Call exec query', 'funckey', funckey, 'Error', result);
                      return res.send(restResult);
                  } else {
                      var EC = 0;
                      var EM = 'Success';
                      var restResult = await RestfullService.genResult(EC, EM, result);
                      sails.log.info(prefix + 'Call exec query', 'funckey', funckey, 'result', restResult);
                      return res.send(restResult);
                  }
              } catch (error) {
                  sails.log.error(prefix + 'Call exec query', 'funckey', funckey, 'Exception', error);
                  return res.send(RestfullService.genResult(-101333, error.message, null));
      
              }
      },
};

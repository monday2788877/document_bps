var path = require('path')
var LogHelper = require(path.resolve(__dirname, '../common/LogHelper'));
module.exports = {
    downloadReport: async function (req, res) {
        try {
            sails.log.info("downloadReport.:Trying to download file for", req.body.link, LogHelper.logReq(req));
            // Hardcode test 
            let link = req.body.link;
            link = sails.config.REPORT_FOLDER + link;
            // link = "files/oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm";
            res.download(link, function (err) {
                if (err) {
                    sails.log.error("downloadReport.:Err", LogHelper.logReq(req, err));
                    return res.serverError("Cannot find requested file! Please contact VSD for support.");
                } else {
                    return res.ok();
                }
            })
        }
        catch (e) {
            sails.log.error("downloadReport.:Error", LogHelper.logReq(req, e));
        }
    },
};
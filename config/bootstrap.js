/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */
var path = require('path')
var connectOra = require(path.resolve(__dirname, '../api/business/connectOraDb.js'));
var DBService = require(path.resolve(__dirname, '../api/business/DBService.js'));
var connCf = require('./env');
var AdminController = require(path.resolve(__dirname, '../api/controllers/AdminController'));
var connectDB2 = require(path.resolve(__dirname, '../api/business/connectDB2.js'));
var DB2Service = require(path.resolve(__dirname, '../api/business/DB2Service.js'));
module.exports.bootstrap = async function (cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  try {
    sails.log.debug('initialise connection pool');
    sails.config.appName = "FUND-BPS";
    var pool = await connectOra.createPool(connCf.db);
    DBService.setPool(pool);
    sails.log.debug('connection pool created!!!!!!!!!!!!!!!!!!!!!'
      + connCf.db.connectString
      + " \nuser=" + connCf.db.user);
    var rs = AdminController.loadStatement();
    sails.log.debug('load statement!!!!!!!!!!!!!!!!!!!!!', rs);

    //DB2
    sails.log.info('initialise connection DB2 pool');
    var poolDB2 = await connectDB2.createPool(connCf.db2);
    await DB2Service.setPool(poolDB2);
    sails.log.info('connection pool DB2 created!!!!!!!!!!!!!!!!!!!!!');

    cb();
  } catch (err) {
    sails.log.error('Error occurred creating database connection pool.:' + connCf.db.connectString, err);
    sails.log.error('Exiting process');
    cb();
  }
};
process.on('uncaughtException', function (err) {
  console.error('Uncaught exception ', err);
});
process.on('unhandledRejection', (err) => {
  console.error('unhandledRejection ', err);
  process.exit(1);
})
// process.on('SIGTERM', function () {
//   sails.log.error('Received SIGTERM');
// });

// process.on('SIGINT', function () {
//   sails.log.error('Received SIGINT');
// });

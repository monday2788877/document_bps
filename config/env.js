/**
 * Database
 */
var connCf = require('./connections');

module.exports = {
//=====config for bps-cb ======
  port: 1363,//port bps-cb
  db: connCf.connections.OracleDBServerCB, //connection bps-cb
  db2: connCf.connections.DB2, //connection bps-cb

  //=====config for bps-fa ======
//port: 1383,//port bps-fa
  //db: connCf.connections.OracleDBServerFA, //connection bps-fa

//=====config for bps-test ======
  //port: 1363,//port bps-test
  //db: connCf.connections.OracleDBServerTest, //connection bps-test

  //=====config for bps default ======
  REDIS_HOST: 'localhost' , //host redis-shb
  REDIS_PORT: 6379, //port redis-shb
  REDIS_PASS: 'tps@1234', //pass redis-shb
  REPORT_FOLDER: '',//report folder root
}